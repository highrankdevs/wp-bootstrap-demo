#!/bin/sh

if [ "$USER" = "vagrant" ]; then
  ./vendor/bin/wp --url='http://highrank.local/' --path='content/wp' db reset
else
  vagrant ssh -c "cd /var/www/highrank && ./vendor/bin/wp --url='http://highrank.local/' --path='content/wp' db reset"
fi